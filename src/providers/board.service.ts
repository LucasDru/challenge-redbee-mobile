import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { Service } from './common/service';

@Injectable()
export class BoardService {
    private baseUrl: string;

    constructor(private http: Http, private service: Service) {
        this.baseUrl = this.service.UrlApi;
    }

    post(model) {
        return this.http.post(`${this.service.UrlApi}/boards`, model, { headers: this.service.getHeaders() }).map((res: Response) => res.json());
    }

    put(model) {
        return this.http.put(`${this.service.UrlApi}/boards`, model, { headers: this.service.getHeaders() }).map((res: Response) => res.json());
    }

    get(userId){
        return this.http.get(`${this.service.UrlApi}/boards/${userId}/tweets`, { headers: this.service.getHeaders() }).map((res: Response) => res.json());
    }
}