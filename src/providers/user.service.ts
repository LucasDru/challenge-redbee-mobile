import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { Service } from './common/service';

@Injectable()
export class UserService {
    private baseUrl: string;

    constructor(private http: Http,
        private service: Service) {
        this.baseUrl = this.service.UrlApi;
    }

    login(username: string, password: string) {
        var json = {
            username: username,
            password: password
        }

        return this.http.post(`${this.service.UrlApi}/login`, json, { headers: this.service.getHeaders() }).map((res: Response) => res.json());
    }

    create(username: string, password: string) {
        var json = {
            username: username,
            password: password
        }

        return this.http.post(`${this.service.UrlApi}/users`, json, { headers: this.service.getHeaders() }).map((res: Response) => res.json());
    }
}