import { Injectable } from '@angular/core';
import { Headers } from '@angular/http';

@Injectable()
export class  Configuration {

    UrlApi : string = "https://challenge-redbee.herokuapp.com/api";
    //UrlApi: string = "http://localhost:9666/api/";
 
    constructor(){
    }

    getHeaders(){
        let headers = new Headers();

        headers.append('Content-Type', 'application/json');
        headers.append('Access-Control-Allow-Origin', '*');
        
        return headers;
    }
}