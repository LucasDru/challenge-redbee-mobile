import { Injectable } from '@angular/core';
import { LoadingController } from 'ionic-angular';

@Injectable()
export class LoaderService {
    
    private loader: any;

    constructor(private loading: LoadingController) {}

    show(){
        this.loader = this.loading.create({           
            content: "Por favor espere..."       
        });

        this.loader.present();
    }

    hide(){
        this.loader.dismiss();
    }
}