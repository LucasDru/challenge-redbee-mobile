import { Injectable } from '@angular/core';
import { Toast, ToastOptions } from '@ionic-native/toast';

@Injectable()
export class MessageService {
    
    private readonly successBackgroud: string = "#51a351";
    private readonly warningBackgroud: string = "#f89406";
    private readonly errorBackgroud: string = "#bd362f";

    constructor(private toast: Toast) {}

    showMessages(messages){
        messages.forEach(element => {
            this.handleShow(element);
        });
    }

    handleShow(element){
        switch(element.type){
            case "Success": this.show(element.text, this.successBackgroud);break;
            case "Error": this.show(element.text, this.errorBackgroud);break;
            case "Warning": this.show(element.text, this.warningBackgroud);break;
        }
    }

    showSuccess(message){
        this.show(message, this.successBackgroud);
    }
    
    showError(message){
        this.show(message, this.errorBackgroud);
    }

    
    showWarning(message){
        this.show(message, this.warningBackgroud);
    }

    show(text, backgroundColor){
        let toastOptions: ToastOptions = {
            message: text,
            duration: 3000,
            position: "center",
            styling: {
                opacity: 1.0,
                backgroundColor: backgroundColor
            }
         };

        this.toast.showWithOptions(toastOptions).subscribe(toast => {});
    }
}