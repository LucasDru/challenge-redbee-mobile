//MODULOS
import { MyApp } from './app.component';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HttpModule } from '@angular/http';
import { Toast } from '@ionic-native/toast';

//PROVIDERS
import { UserService } from '../providers/user.service';
import { Configuration } from "../providers/common/configuration";
import { ErrorHandlerService } from "../providers/common/errorHandler.service";
import { Service } from "../providers/common/service";
import { MessageService } from '../providers/common/message.service';

//Paginas
import { HomePage } from '../pages/home/home';
import { LoginPage } from "../pages/login/login";
import { BoardService } from '../providers/board.service';
import { NewUserPage } from '../pages/new-user/new-user';
import { NewBoardPage } from '../pages/new-board/new-board';
import { FormBoard } from '../pages/form-board/form-board';
import { EditBoardPage } from '../pages/edit-board/edit-board';
import { LoaderService } from '../providers/common/loader.service';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    NewBoardPage,
    NewUserPage,
    FormBoard,
    EditBoardPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp, {
      iconMode: 'md',
      mode: 'md'
    }),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    NewBoardPage,
    NewUserPage,
    EditBoardPage,
    FormBoard
  ],
  providers: [
    Toast,
    SplashScreen,
    Configuration,
    Service,
    LoaderService,
    ErrorHandlerService,
    UserService,
    MessageService,
    BoardService,
    { provide: ErrorHandler, useClass: IonicErrorHandler }
  ]
})
export class AppModule { }
