import { Component, ViewChild } from '@angular/core';
import { Platform, Nav } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HomePage } from '../pages/home/home';
import { LoginPage } from "../pages/login/login";
import { NewBoardPage } from '../pages/new-board/new-board';

export interface MenuItem {
  title: string;
  component: any;
  icon: string;
}

@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  @ViewChild(Nav) nav: Nav;
  private _platform: Platform;
  public rootPage: any = LoginPage;
  public appMenuItems: Array<MenuItem>;

  constructor(platform: Platform,
              splashScreen: SplashScreen) {

    this._platform = platform;

    this._platform.ready().then(() => {
      splashScreen.hide();
    });

    this.appMenuItems = [
      { title: 'Inicio', component: HomePage, icon: 'home' },
      { title: 'Crear Board', component: NewBoardPage, icon: 'plus' }
    ];
  }

  openPage(page) {
    if (page.title == 'Inicio') {
      this.nav.setRoot(page.component);
    }
    else {
      this.nav.push(page.component);
    }
  }

  logout() {
    this.nav.setRoot(LoginPage);
  }
}