import { Component, OnInit } from "@angular/core";
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { NavController, MenuController } from "ionic-angular";
import { HomePage } from "../home/home";
import { Subscription } from "rxjs/Subscription";
import { OnDestroy } from "@angular/core/src/metadata/lifecycle_hooks";
import { ErrorHandlerService } from "../../providers/common/errorHandler.service";
import { NewUserPage } from "../new-user/new-user";
import { UserService } from "../../providers/user.service";
import { LoaderService } from "../../providers/common/loader.service";

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage implements OnInit, OnDestroy {

  public onLoginForm: FormGroup;
  public userName: string;
  public password: string;

  loginSubscrip: Subscription;

  constructor(private _fb: FormBuilder,
    public nav: NavController,
    public menu: MenuController,
    private loader: LoaderService,
    private userService: UserService,
    private errorHandlerService: ErrorHandlerService) {

    this.menu.swipeEnable(false);
  }

  ngOnInit() {
    this.onLoginForm = this._fb.group({
      userName: ['', Validators.compose([
        Validators.required
      ])],
      password: ['', Validators.compose([
        Validators.required
      ])]
    });
  }

  ngOnDestroy(): void {
    if (this.loginSubscrip) this.loginSubscrip.unsubscribe();
  }

  login() {
    this.loader.show();

    this.loginSubscrip = this.userService.login(this.userName, this.password).subscribe(
      data => {
        this.loader.hide();
        this.onLoginSucces(data);
      },
      error => {
        this.loader.hide();
        this.errorHandlerService.handleErrors(error);
      });
  }

  onLoginSucces(data) {
    localStorage.setItem('username', this.userName);
    this.nav.setRoot(HomePage);
  }

  newUser(){
    this.nav.push(NewUserPage);
  }
}
