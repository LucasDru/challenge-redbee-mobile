import { Component, OnDestroy, ViewChild } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Subscription } from 'rxjs/Subscription';
import { ErrorHandlerService } from '../../providers/common/errorHandler.service';
import { BoardService } from '../../providers/board.service';
import { MessageService } from '../../providers/common/message.service';
import { LoaderService } from '../../providers/common/loader.service';

@Component({
  selector: 'page-new-board',
  templateUrl: 'new-board.html'
})
export class NewBoardPage implements OnDestroy {

  @ViewChild("form") form;

  newBoardSubscrip: Subscription;

  constructor(public navCtrl: NavController,
              private boardService: BoardService,   
              private loader: LoaderService,
              private messageService: MessageService,
              private errorHandlerService: ErrorHandlerService) {

  }  

  ngOnDestroy(): void {
      if (this.newBoardSubscrip) this.newBoardSubscrip.unsubscribe();
  }

  create() {
    var json = {
      username: localStorage.getItem('username'),
      title: this.form.title,
      peoples: this.form.peoples.join(";"),
      interests: this.form.interests.join(";")
    }

    this.loader.show();

    this.newBoardSubscrip = this.boardService.post(json).subscribe(
      response => {
        this.loader.hide();
        if(response.messages) this.messageService.showMessages(response.messages);
        this.navCtrl.pop();
      },
      error => {
        this.loader.hide();
        this.errorHandlerService.handleErrors(error);
      });
  }
}
