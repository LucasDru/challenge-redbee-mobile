import { Component, OnInit, OnDestroy } from '@angular/core';
import { NavController } from 'ionic-angular';
import { BoardService } from '../../providers/board.service';
import { Subscription } from 'rxjs/Subscription';
import { ErrorHandlerService } from '../../providers/common/errorHandler.service';
import { EditBoardPage } from '../edit-board/edit-board';
import { LoaderService } from '../../providers/common/loader.service';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit, OnDestroy {  

  getSubscript: Subscription;

  public boards: any[] = new Array();

  constructor(public navCtrl: NavController,
              private loader: LoaderService,
              private errorHandlerService: ErrorHandlerService,
              private boardService: BoardService) {
  }    

  ngOnInit(): void {
    this.fillBoards();
    this.getTweets();
  }

  ngOnDestroy(): void {
    if(this.getSubscript) this.getSubscript.unsubscribe();
  }

  updateBoard(board){
    localStorage.setItem("board", JSON.stringify(board));
    this.navCtrl.push(EditBoardPage);
  }

  getTweets(){
    var username = localStorage.getItem('username');

    this.loader.show();

    this.getSubscript = this.boardService.get(username).subscribe(response => {
      this.loader.hide();

      if(response && response.length > 0){
        response.forEach(boardFromServer => {
          
          var board = this.boards.find(x => x.boardId == boardFromServer.boardId);

          if(board){
            boardFromServer.tweets.forEach(tweet => {
              tweet.new = true;
              board.tweets.push(tweet);
            });
          }
          else{
            this.boards.push(boardFromServer);
          }
        });
      }

      localStorage.setItem(username, JSON.stringify(this.boards));
    }, 
    error => {
      this.loader.hide();
      this.errorHandlerService.handleErrors(error);
    });
  }

  fillBoards(){
    var username = localStorage.getItem('username');

    var boards = JSON.parse(localStorage.getItem(username));
    if(boards){
      boards.forEach(board => {
        board.tweets.forEach(tweet => {
          tweet.new = false;
        });
      });

      this.boards = boards;
    }
  }
}
