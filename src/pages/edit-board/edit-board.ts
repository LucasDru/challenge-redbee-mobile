import { Component, OnDestroy, ViewChild, OnInit } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Subscription } from 'rxjs/Subscription';
import { ErrorHandlerService } from '../../providers/common/errorHandler.service';
import { BoardService } from '../../providers/board.service';
import { MessageService } from '../../providers/common/message.service';
import { LoaderService } from '../../providers/common/loader.service';

@Component({
  selector: 'page-edit-board',
  templateUrl: 'edit-board.html'
})
export class EditBoardPage implements OnInit, OnDestroy {

  @ViewChild("form") form;

  editBoardSubscrip: Subscription;

  constructor(public navCtrl: NavController,
              private boardService: BoardService,   
              private loader: LoaderService,
              private messageService: MessageService,
              private errorHandlerService: ErrorHandlerService) {

  }  
  
  ngOnInit(): void {
    var board = JSON.parse(localStorage.getItem("board"));
    this.form.setModel(board);
  }

  ngOnDestroy(): void {
      if (this.editBoardSubscrip) this.editBoardSubscrip.unsubscribe();
  }

  update() {
    var json = {
      username: localStorage.getItem('username'),
      title: this.form.title,
      boardId: this.form.boardId,
      peoples: this.form.peoples.join(";"),
      interests: this.form.interests.join(";")
    }

    this.loader.show();
    
    this.editBoardSubscrip = this.boardService.put(json).subscribe(
      response => {
        this.loader.hide();
        if(response.messages) this.messageService.showMessages(response.messages);
        this.navCtrl.pop();
      },
      error => {
        this.loader.hide();
        this.errorHandlerService.handleErrors(error);
      });
  }
}
