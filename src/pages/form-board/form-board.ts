import { Component } from '@angular/core';

@Component({
  selector: 'form-board',
  templateUrl: 'form-board.html'
})
export class FormBoard {

  public boardId: string;
  public title: string;
  public interest: string;
  public people: string;

  public interests: string[] = new Array();
  public peoples: string[] = new Array();

  constructor() {}  

  setModel(board){
    this.boardId = board.boardId;
    this.title = board.title;
    this.interests = board.interests;
    this.peoples = board.peoples;
  }

  addInterest(){
    this.interests.push("#" + this.interest);
    this.interest = "";
  }

  removeInterest(index){
    this.interests.splice(index, 1);
  }

  addPeople(){
    this.peoples.push("@" + this.people);
    this.people = "";
  }

  removePeople(index){
    this.peoples.splice(index, 1);
  }
}
