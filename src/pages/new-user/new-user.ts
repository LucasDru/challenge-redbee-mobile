import { Component, OnInit, OnDestroy } from '@angular/core';
import { NavController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { UserService } from '../../providers/user.service';
import { ErrorHandlerService } from '../../providers/common/errorHandler.service';
import { MessageService } from '../../providers/common/message.service';
import { LoaderService } from '../../providers/common/loader.service';

@Component({
  selector: 'page-new-user',
  templateUrl: 'new-user.html'
})
export class NewUserPage implements OnInit, OnDestroy {

    public onNewUserForm: FormGroup;

    public username: string;
    public password: string;

    newUserSubscrip: Subscription;

    constructor(public navCtrl: NavController,
                private userService: UserService,
                private messageService: MessageService,
                private loader: LoaderService,
                private errorHandlerService: ErrorHandlerService,
                private formBuilder: FormBuilder) {

    }  

    ngOnInit(): void {
        this.onNewUserForm = this.formBuilder.group({
            userName: ['', Validators.compose([
                Validators.required
            ])],
            password: ['', Validators.compose([
                Validators.required
            ])]
        });
    }

    ngOnDestroy(): void {
        if (this.newUserSubscrip) this.newUserSubscrip.unsubscribe();
    }

    create() {
        this.loader.show();

        this.newUserSubscrip = this.userService.create(this.username, this.password).subscribe(
            response => {
                this.loader.hide();
                if(response.messages) this.messageService.showMessages(response.messages);
                this.navCtrl.pop();
            },
            error => {
                this.loader.hide();
                this.errorHandlerService.handleErrors(error);
            });
    }
  
 }
